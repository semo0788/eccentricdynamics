# This file will plot the values generated by edisk

import numpy as np
import matplotlib.pyplot as plt

def main():
	rawData = np.genfromtxt('./OUT/orbits.txt', comments='#', delimiter=',').T
	N, energy, enc, senc, inc, sinc, Omega, sOmega, omega, somega = rawData
	
	plt.figure(1)
	plt.style.use('ggplot')
	# Plot the eccentricity
	incfig = plt.subplot2grid((5, 1), (0, 0))
	incfig.set_title('Average Eccentricity')
	incfig.set_xlabel('Orbit')
	incfig.set_ylabel('Eccentricity')
	incfig.plot(N, enc)
	incfig.plot(N, enc + senc, c='b')
	incfig.plot(N, enc - senc, c='b')
	# Plot the inclination
	incfig = plt.subplot2grid((5, 1), (1, 0))
	incfig.set_title('Average Inclination')
	incfig.set_xlabel('Orbit')
	incfig.set_ylabel('Inclination (radians)')
	incfig.plot(N, inc)
	incfig.plot(N, inc + sinc, c='b')
	incfig.plot(N, inc - sinc, c='b')
	# Plot the longitude of ascending node
	Omegafig = plt.subplot2grid((5, 1), (2, 0))
	Omegafig.set_title('Average Longitude of Ascending Node')
	Omegafig.set_xlabel('Orbit')
	Omegafig.set_ylabel('LoAN (radians)')
	Omegafig.plot(N, Omega)
	Omegafig.plot(N, Omega + sOmega, c='b')
	Omegafig.plot(N, Omega - sOmega, c='b')
	# Plot the argument of periapsis
	omegafig = plt.subplot2grid((5, 1), (3, 0))
	omegafig.set_title('Average Argument of Pericenter')
	omegafig.set_xlabel('Orbit')
	omegafig.set_ylabel('AoP (radians)')
	omegafig.plot(N, omega)
	omegafig.plot(N, omega + somega, c='b')
	omegafig.plot(N, omega - somega, c='b')
	# Plot the system energy
	efig = plt.subplot2grid((5, 1), (4, 0))
	efig.set_title('Total System Energy')
	efig.set_xlabel('Orbit')
	efig.set_ylabel('Energy')
	efig.plot(N, energy)

	# Prepare and show
	#plt.tight_layout()
	plt.show()



if __name__ == '__main__':
	main()
