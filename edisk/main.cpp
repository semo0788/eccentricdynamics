#include <common.hpp>

const double MASS_BH = 1; // Solar Mass Units
const double MASS_STAR = 1e-4; // Solar Mass Units
const int NUM_STAR = 50; // Number of stars in disk
const double ORBIT_TIME = 2 * M_PI; // The total amount of scaled time that represents a single orbit at a = 1

/*
	Calculates the cartesian components of the Keplerian Orbits, and populates the passed particle with these components.
	Components are calculated assuming the xy-plane is the reference plane, and the +x axis is the reference direction.
	Paramaters:
	  a      -  Semimajor axis
	  e      -  Eccentricity
	  i      -  Inclination
	  omega  -  Argument of Periapsis
	  Omega  -  Longitude of Ascending Node
	  nu     -  True Anomaly
 */
void particle_from_kepler_elements(double a, double e, double i, double omega, double Omega, double nu, reb_particle* part);
double linear_random(double low, double high);
double gaussian_random(double center, double stddev);

// File operations
void open_logfile(const char *name);
void write_orbit(int n, double te, double e, double se, double inc, double sinc, double Omega, double sOmega, double omega, double somega);
void close_logfile();

void calculate_normal_distribution(const double * const values, const int N, double *outsum, double *outavg, double *outstd);

void heartbeat_callback(reb_simulation * const sim);

const reb_particle* BH_PARTICLE = nullptr;
int main(int argc, char **argv)
{
	reb_simulation* sim = reb_create_simulation();
	sim->G = 1;
	//sim->dt = 1e-4;
	sim->integrator = reb_simulation::REB_INTEGRATOR_IAS15;
	sim->heartbeat = [] (reb_simulation * const sim) -> void { throw std::runtime_error(""); };
	//sim->ri_ias15.min_dt = 1e-9;

	// Add the black hole
	reb_particle bh;
	bh.m = MASS_BH;
	reb_add(sim, bh);
	BH_PARTICLE = &sim->particles[0]; // Save pointer to black hole to check for later

	// Add the stars
	for (int ii = 0; ii < NUM_STAR; ++ii)
	{
		reb_particle star;
		star.m = MASS_STAR;

		double a = linear_random(1.3, 1.7);
		double e = 0.7;
		double i = gaussian_random(0, 3e-2); // This puts 0 +/- 1e-2 within 9 sigma of the mean
		double omega = 0;
		double Omega = 0;
		double M = linear_random(0, 360);

		particle_from_kepler_elements(a, e, i, omega, Omega, M, &star);
		reb_add(sim, star);
	}

	reb_move_to_com(sim);
	open_logfile("orbits.txt");
	reb_integrate(sim, ORBIT_TIME * 10000); // Do N orbits
	close_logfile();

	reb_free_simulation(sim);
	return 0;
}



/*
	Heartbeat function called at the end of each timestep.
 */
double currentTime = 0;
int orbitCount = 0;
void heartbeat_callback(reb_simulation * const sim)
{
	std::cout << "Heartbeat" << std::endl;
	double lastdt = sim->dt_last_done;
	currentTime += lastdt;
	if (currentTime >= ORBIT_TIME) // If we have completed another orbit
	{
		std::cout << "Orbit: " << (++orbitCount) << std::endl;
		currentTime = 0;

		const double simEnergy = reb_tools_energy(sim);
		std::cout << "\tTotal Energy: " << simEnergy << std::endl;

		reb_particle *plist = sim->particles;
		const int pcount = sim->N;
		double p_inc[NUM_STAR] = { 0 };
		double p_Om[NUM_STAR] = { 0 };
		double p_om[NUM_STAR] = { 0 };
		double p_e[NUM_STAR] = { 0 };
		int p_index = 0;
		for (int i = 0; i < pcount; ++i)
		{
			const reb_particle& particle = plist[i];
			if (&particle == BH_PARTICLE) // Compare pointers to skip over the black hole
				continue;

			const reb_orbit orbit = reb_tools_particle_to_orbit(1, particle, *BH_PARTICLE);
			//const reb_vec3d rvec = reb_vec3d_make(particle.x, particle.y, particle.z);
			//const reb_vec3d vvec = reb_vec3d_make(particle.vx, particle.vy, particle.vz);

			//const double c1 = reb_vec3d_lensq(vvec) - (1.0 / reb_vec3d_len(rvec));
			//const double c2 = reb_vec3d_dot(rvec, vvec);
			//const reb_vec3d evec = c1 * rvec - c2 * vvec;

			p_e[p_index] = orbit.e;
			p_inc[p_index] = orbit.inc;
			p_Om[p_index] = orbit.Omega;
			p_om[p_index++] = orbit.omega;
		}

		double eavg, estd, iavg, istd, Oavg, Ostd, oavg, ostd;
		calculate_normal_distribution(p_e, NUM_STAR, nullptr, &eavg, &estd);
		calculate_normal_distribution(p_inc, NUM_STAR, nullptr, &iavg, &istd);
		calculate_normal_distribution(p_Om, NUM_STAR, nullptr, &Oavg, &Ostd);
		calculate_normal_distribution(p_om, NUM_STAR, nullptr, &oavg, &ostd);
		write_orbit(orbitCount, simEnergy, eavg, estd, iavg, istd, Oavg, Ostd, oavg, ostd);
	}
}



/*
	Implementation of this method was developed from two sources:
	  1) https://en.wikipedia.org/wiki/Orbital_elements
	  2) https://en.wikipedia.org/wiki/Mean_motion 
	  3) http://ccar.colorado.edu/asen5070/handouts/kep2cart_2002.doc
 */
void particle_from_kepler_elements(double a, double e, double i, double omega, double Omega, double nu, reb_particle* part)
{
	// Compute the current radius of the particle
	double p = (a * (1 - (e * e)));
	double r = p / (1 + (e * cos(nu)));

	// Compute commonly used cosines and sines
	double cO = cos(Omega), sO = sin(Omega);
	double ci = cos(i), si = sin(i);
	double co = cos(omega + nu), so = sin(omega + nu);
	double sn = sin(nu);

	// Compute the position coordinates
	double x = r * ((cO * co) - (sO * so * ci));
	double y = r * ((sO * co) + (cO * so * ci));
	double z = r * (si * so);

	// Compute the "specific angular momentum"
	const static double mu = MASS_BH + MASS_STAR; // * G, technically, but G = 1
	double h = sqrt(mu * p);

	// Compute commonly used constants for the velocity calculations
	double herp = ((h * e) / (r * p)) * sn;
	double hr = h / r;

	// Compute the velocity coordinates
	double vx = (x * herp) - (hr * ((cO * so) + (sO * co * ci)));
	double vy = (y * herp) - (hr * ((sO * so) - (cO * co * ci)));
	double vz = (z * herp) + (hr * si * co);

	// Populate the particle
	part->x = x;
	part->y = y;
	part->z = z;
	part->vx = vx;
	part->vy = vy;
	part->vz = vz;
}


double linear_random(double low, double high)
{
	static std::default_random_engine rEngine;
	
	std::uniform_real_distribution<double> dist(low, high);
	return dist(rEngine);
}

double gaussian_random(double center, double stddev)
{
	static std::default_random_engine rEngine;

	std::normal_distribution<double> dist(center, stddev);
	return dist(rEngine);
}


// ================================================================================================

std::ofstream logfile;
void open_logfile(const char *name)
{
	logfile.open(name, std::ios::out | std::ios::trunc);
	if (!logfile.is_open())
	{
		std::cerr << "Could not open log file." << std::endl;
		throw new std::runtime_error("Could not open log file.");
	}
	logfile << "# Orbit Number, Total Energy, Avg. e, Std. e, Avg. i, Std i., Avg. Omega, Std. Omega, Avg. omega, Std. omega" << std::endl;
}

void write_orbit(int n, double te, double e, double se, double inc, double sinc, double Omega, double sOmega, double omega, double somega)
{
	logfile << n << ", " << te << ", " << e << ", " << se << ", " << inc << ", " << sinc << ", " << Omega << ", " << sOmega << ", " 
		<< omega << ", " << somega << std::endl;
}

void close_logfile()
{
	logfile.close();
}

// ================================================================================================

void calculate_normal_distribution(const double * const values, const int N, double *outsum, double *outavg, double *outstd)
{
	double sum = 0;
	for (int i = 0; i < N; ++i) { sum += values[i]; }

	if (outsum) *outsum = sum;

	const double avg = sum / N;
	sum = 0;
	for (int i = 0; i < N; ++i)
	{
		const double diff = values[i] - avg;
		sum += (diff * diff);
	}

	const double stddev = sqrt(sum / N);

	if (outavg) *outavg = avg;
	if (outstd) *outstd = stddev;
}
