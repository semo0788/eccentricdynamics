/*
    This file includes the rebound C++ header, as well as the most commonly used C++
    standard library header files. Also some standard using directives.
 */

#include "rebound.hpp"

#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>

#include <stdlib.h>
#include <cstdint>
#include <cfloat>
#include <exception>
#include <stdexcept>

#include <algorithm>
#include <array>
#include <vector>
#include <unordered_map>
#include <stack>
#include <queue>
#include <list>
#include <deque>

#include <cmath>
#include <random>

using uint8 = std::uint8_t;
using uint16 = std::uint16_t;
using uint32 = std::uint32_t;
using uint64 = std::uint64_t;
using int8 = std::int8_t;
using int16 = std::int16_t;
using int32 = std::int32_t;
using int64 = std::int64_t;

using flt32 = std::float_t;
using flt64 = std::double_t;

#include "vecops.hpp"