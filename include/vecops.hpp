/*
	This file defines operations on the reb_vec3d object defined in rebound,
	because rebound does not expose the functions to manipulate its own vectors.
 */

#include "rebound.hpp"
#include <cmath>
#include <iostream>
#include <string>
#include <sstream>

inline reb_vec3d reb_vec3d_make(double x, double y, double z)
{
	reb_vec3d ret;
	ret.x = x;
	ret.y = y;
	ret.z = z;
	return ret;
}

// ================================================================================================

inline reb_vec3d operator + (const reb_vec3d& v1, const reb_vec3d& v2)
{
	return reb_vec3d_make(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

inline reb_vec3d operator - (const reb_vec3d& v1, const reb_vec3d& v2)
{
	return reb_vec3d_make(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

inline reb_vec3d operator * (const reb_vec3d& v1, const reb_vec3d& v2)
{
	return reb_vec3d_make(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
}

inline reb_vec3d operator / (const reb_vec3d& v1, const reb_vec3d& v2)
{
	return reb_vec3d_make(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
}

inline reb_vec3d& operator += (reb_vec3d& v1, const reb_vec3d& v2)
{
	return (v1 = reb_vec3d_make(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z));
}

inline reb_vec3d& operator -= (reb_vec3d& v1, const reb_vec3d& v2)
{
	return (v1 = reb_vec3d_make(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z));
}

inline reb_vec3d& operator *= (reb_vec3d& v1, const reb_vec3d& v2)
{
	return (v1 = reb_vec3d_make(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z));
}

inline reb_vec3d& operator /= (reb_vec3d& v1, const reb_vec3d& v2)
{
	return (v1 = reb_vec3d_make(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z));
}

// ================================================================================================

inline reb_vec3d operator + (const reb_vec3d& v1, const double d)
{
	return reb_vec3d_make(v1.x + d, v1.y + d, v1.z + d);
}

inline reb_vec3d operator - (const reb_vec3d& v1, const double d)
{
	return reb_vec3d_make(v1.x - d, v1.y - d, v1.z - d);
}

inline reb_vec3d operator * (const reb_vec3d& v1, const double d)
{
	return reb_vec3d_make(v1.x * d, v1.y * d, v1.z * d);
}

inline reb_vec3d operator / (const reb_vec3d& v1, const double d)
{
	return reb_vec3d_make(v1.x / d, v1.y / d, v1.z / d);
}

//
inline reb_vec3d operator * (const double d, const reb_vec3d& v1)
{
	return reb_vec3d_make(v1.x * d, v1.y * d, v1.z * d);
}

inline reb_vec3d operator / (const double d, const reb_vec3d& v1)
{
	return reb_vec3d_make(d / v1.x, d / v1.y, d / v1.z);
}
//

inline reb_vec3d& operator += (reb_vec3d& v1, const double d)
{
	return (v1 = reb_vec3d_make(v1.x + d, v1.y + d, v1.z + d));
}

inline reb_vec3d& operator -= (reb_vec3d& v1, const double d)
{
	return (v1 = reb_vec3d_make(v1.x - d, v1.y - d, v1.z - d));
}

inline reb_vec3d& operator *= (reb_vec3d& v1, const double d)
{
	return (v1 = reb_vec3d_make(v1.x * d, v1.y * d, v1.z * d));
}

inline reb_vec3d& operator /= (reb_vec3d& v1, const double d)
{
	return (v1 = reb_vec3d_make(v1.x / d, v1.y / d, v1.z / d));
}

// ================================================================================================

inline double reb_vec3d_len(const reb_vec3d& v)
{
	return sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

inline double reb_vec3d_lensq(const reb_vec3d& v)
{
	return v.x * v.x + v.y * v.y + v.z * v.z;
}

inline double reb_vec3d_dot(const reb_vec3d& v1, const reb_vec3d& v2)
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

inline reb_vec3d reb_vec3d_cross(const reb_vec3d& v1, const reb_vec3d& v2)
{
	double nx = v1.y * v2.z - v1.z * v2.y;
	double ny = v1.z * v2.x - v1.x * v2.z;
	double nz = v1.x * v2.y - v1.y * v2.x;
	return reb_vec3d_make(nx, ny, nz);
}

// ================================================================================================

inline std::string reb_vec3d_str(const reb_vec3d& v)
{
	std::stringstream ss;
	ss << "{ " << v.x << ", " << v.y << ", " << v.z << " }";
	return ss.str();
}

inline std::ostream& operator << (std::ostream& os, const reb_vec3d& v)
{
	os << "{ " << v.x << ", " << v.y << ", " << v.z << " }";
	return os;
}