/* 
    This file exists to allow for c++ wrapping of the rebound code.
 */

#ifdef __cplusplus

// The "restrict" keyword does not exist in C++, so redefine it to use the __restrict__ keyword
//     specific to g++
#   define restrict __restrict__

extern "C" {
#endif // __cplusplus

#include <reboundx.h>

#ifdef __cplusplus
}
#endif // __cplusplus